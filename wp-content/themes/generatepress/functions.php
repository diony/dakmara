<?php
/**
 * GeneratePress.
 *
 * Please do not make any edits to this file. All edits should be done in a child theme.
 *
 * @package GeneratePress
 */


if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

if (!defined('THEME_DIR')) {
    define('THEME_DIR', dirname(__FILE__));
}

// Set our theme version.
define('GENERATE_VERSION', '2.4.2');

if (!function_exists('generate_setup')) {
    add_action('after_setup_theme', 'generate_setup');
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * @since 0.1
     */
    function generate_setup()
    {
        // Make theme available for translation.
        load_theme_textdomain('generatepress');

        // Add theme support for various features.
        add_theme_support('automatic-feed-links');
        add_theme_support('post-thumbnails');
        add_theme_support('post-formats', array('aside', 'image', 'video', 'quote', 'link', 'status'));
        add_theme_support('woocommerce');
        add_theme_support('title-tag');
        add_theme_support('html5', array('search-form', 'comment-form', 'comment-list', 'gallery', 'caption'));
        add_theme_support('customize-selective-refresh-widgets');
        add_theme_support('align-wide');
        add_theme_support('responsive-embeds');

        add_theme_support('custom-logo', array(
            'height' => 70,
            'width' => 350,
            'flex-height' => true,
            'flex-width' => true,
        ));

        // Register primary menu.
        register_nav_menus(array(
            'primary' => __('Primary Menu', 'generatepress'),
        ));

        /**
         * Set the content width to something large
         * We set a more accurate width in generate_smart_content_width()
         */
        global $content_width;
        if (!isset($content_width)) {
            $content_width = 1200; /* pixels */
        }

        // This theme styles the visual editor to resemble the theme style.
        add_editor_style('css/admin/editor-style.css');

    }
}


/**
 * Get all necessary theme files
 */
$theme_dir = get_template_directory();

require $theme_dir . '/inc/theme-functions.php';
require $theme_dir . '/inc/defaults.php';
require $theme_dir . '/inc/class-css.php';
require $theme_dir . '/inc/css-output.php';
require $theme_dir . '/inc/general.php';
require $theme_dir . '/inc/customizer.php';
require $theme_dir . '/inc/markup.php';
require $theme_dir . '/inc/typography.php';
require $theme_dir . '/inc/plugin-compat.php';
require $theme_dir . '/inc/block-editor.php';
//require $theme_dir . '/inc/migrate.php';
require $theme_dir . '/inc/deprecated.php';

if (is_admin()) {
    require $theme_dir . '/inc/meta-box.php';
    require $theme_dir . '/inc/dashboard.php';
}

/**
 * Load our theme structure
 */
require $theme_dir . '/inc/structure/archives.php';
require $theme_dir . '/inc/structure/comments.php';
require $theme_dir . '/inc/structure/featured-images.php';
require $theme_dir . '/footer_actual.php';
require $theme_dir . '/inc/structure/header.php';
require $theme_dir . '/inc/structure/navigation.php';
require $theme_dir . '/inc/structure/post-meta.php';
require $theme_dir . '/inc/structure/sidebars.php';


function categorias_style_temp()
{

    //CSS
  
    wp_enqueue_style('bootstrap_general', get_theme_file_uri('assets/libraries/bootstrap/css/bootstrap.min.css'), array());
    wp_enqueue_style('bootstrap_mdb1', get_theme_file_uri('assets/libraries/mdb/css/mdb.css'), array());
    wp_enqueue_style('custom-style-s', get_theme_file_uri('assets/css/custom.css'), array());
    //wp_enqueue_style('bootstrap_style', get_theme_file_uri('assets/css/bootstrap/bootstrap-grid.min.css'), array());

    //script

    wp_register_script('jquery', get_template_directory_uri() . '/assets/libraries/jquery/js/jquery.min.js', array(), true);
    wp_enqueue_script('jquery');


    wp_register_script('bootstrap_js', get_template_directory_uri() . '/assets/libraries/bootstrap/js/bootstrap.min.js', array(), true);
    wp_enqueue_script('bootstrap_js');

    wp_register_script('popper', get_template_directory_uri() . '/assets/libraries/mdb/js/popper.min.js', array(), true);
    wp_enqueue_script('popper');

    wp_enqueue_script('mdb-script', get_template_directory_uri() . '/assets/libraries/mdb/js/mdb.js', array(), '4.3.1', true); 
   // wp_enqueue_script('mdb-script', get_template_directory_uri() . '/assets/libraries/mdb/js/compiled.min.js', array(), '4.3.1', true);

   /* wp_register_script('mdb', get_template_directory_uri() . '/assets/libraries/mdb/js/mdb.js', array(), true);
    wp_enqueue_script('mdb');*/

    wp_register_script('wow', get_template_directory_uri() . '/assets/libraries/mdb/js/wow.js', array(), true);
    wp_enqueue_script('wow');

    wp_register_script('scriptmdb', get_template_directory_uri() . '/assets/libraries/mdb/js/scriptmdb.js', array(), true);
    wp_enqueue_script('scriptmdb');

    wp_register_script('scrolling_navbar', get_template_directory_uri() . '/assets/libraries/mdb/js/scrolling-navbar.js', array(), true);
    wp_enqueue_script('scrolling_navbar');

    wp_register_script('utiles', get_template_directory_uri() . '/assets/js/utiles.js', array(), true);
    wp_enqueue_script('utiles');

    $wnm_custom = array('template_url' => get_bloginfo('template_url'));
    wp_localize_script('custom-js', 'wnm_custom', $wnm_custom);
    
}

add_action('wp_enqueue_scripts', 'categorias_style_temp', 99);


add_filter('woocommerce_add_to_cart_fragments', function ($fragments) {

    ob_start();
    ?>
    <?php echo WC()->cart->get_cart_contents_count(); ?>


    <?php $fragments['cart-total-contents'] = ob_get_clean();

    return $fragments;

});

add_filter('gettext', 'bbloomer_translate_woocommerce_strings', 999, 3);
function bbloomer_translate_woocommerce_strings($translated, $untranslated, $domain)
{

    if (!is_admin() && 'woocommerce' === $domain) {

        switch ($translated) {

            case 'Proceed to checkout' :
                $translated = 'Pasa por la Caja';
                break;

            case 'Product' :
                $translated = 'Producto';
                break;

            case 'Price' :
                $translated = 'Precio';
                break;

            case 'Quantity' :
                $translated = 'Cantidad';
                break;

            case 'Product' :
                $translated = 'Producto';
                break;

            case 'Add to cart' :
                $translated = 'Agregar';
                break;

            // ETC

        }

    }

    return $translated;

}

add_action( 'woocommerce_shop_loop_item_title', 'bbloomer_woocommerce_product_excerpt', 11 );
function bbloomer_woocommerce_product_excerpt() {
	global $post;
	global $wpdb;
	$current_user = wp_get_current_user();	
	$product = wc_get_product( $post->ID );

    $description =  $post->post_excerpt;

	//$precio_base_user = getPriceBase($product,$current_user->ID);
	
		/*if ( is_user_logged_in() ) {
			echo '<span class="price">';						
			if(!empty($precio_base_user)){
				echo wc_price($precio_base_user);
			 }else{
				echo wc_price($product->get_regular_price()); 
			 }
			 
			echo '</span>';
		}*/

        echo $description;
}

add_action( 'generate_header-category', 'woocommerce_category_image', 2 );
function woocommerce_category_image() {
   
     global $wp_query;
     $cat = $wp_query->get_queried_object();
     $thumbnail_id = get_woocommerce_term_meta( $cat->term_id, 'thumbnail_id', true );
     $image = wp_get_attachment_url( $thumbnail_id );
     //echo $image; die;
     $style = "background-image: url(".$image.");background-size:cover;background-position: center;";
     $html = "";
     if ( $image ) {
        $html .= '<div class="bg-image p-5 text-center shadow-1-strong text-white" style="'.$style.'">
            <h1 class="mb-3 h2">'.$cat->name.'</h1>          
        </div>';
     }

     echo $html; 
}

add_action( 'generate_header-tira-info', 'header_tira_info', 2 );
function header_tira_info() {

            $args = array(  
                'post_type' => 'info_item',
                'post_status' => 'publish',
                'posts_per_page' => 8, 
                'orderby' => 'title', 
                'order' => 'ASC', 
            );
            $query1 = new WP_Query( $args );
          
    $html = '<div class="container-tira-info container">';
    $html .= '<div class="ticker">                            
                    <div class="news d-block d-md-none">
                        <marquee class="news-content">';
                        $html .= '<span>*</span>';
                        while ( $query1->have_posts() ) {   
                            $query1->the_post();
                            $html .= '<b>'.get_the_Title().'</b> : '. get_the_excerpt();
                            $html .= '<span>*</span>';
                        } 

            $html .= '</marquee>';           
          $html .= '</div>';
          $html .= '<div class="news d-none d-md-block">';
                 $html .= '<div class="row item-tira justify-content-between">';

                 while ( $query1->have_posts() ) {   
                    $query1->the_post();
                    $html .= '<div class="col-6"><b>'.get_the_Title().'</b> : '. get_the_excerpt()  . '</div>';                  
                } 

                 
               
                $html .= '</div>';
          $html .= '</div>';
          

    $html .= '</div>';
    $html .= '</div>';
    wp_reset_query();
    echo $html;
}


add_action( 'get_sliders_seccion_pager', 'get_sliders_seccion_pager', 2 );
function get_sliders_seccion_pager() {

    $args = array(  
        'post_type' => 'slider_item',
        'post_status' => 'publish',
        'posts_per_page' => 8, 
        'orderby' => 'title', 
        'order' => 'ASC', 
    );        
    $query = new WP_Query( $args );

    $html = '';
    while ( $query->have_posts() ) {           
        $query->the_post();
        $classActive = ($query->current_post == 0) ? 'active' : '';
        $html .= '<li data-target="#carousel-example-1" data-slide-to="'.$query->current_post.'" class="'.$classActive.'"></li>';

    }
    wp_reset_query();
    echo $html;
}

add_action( 'get_sliders_seccion', 'get_sliders_seccion', 2 );
function get_sliders_seccion() {

        $args = array(  
            'post_type' => 'slider_item',
            'post_status' => 'publish',
            'posts_per_page' => 8, 
            'orderby' => 'title', 
            'order' => 'ASC', 
        );        
        $query = new WP_Query( $args );

    $html = '';
    while ( $query->have_posts() ) {           
        $query->the_post();
       
        $classActive = ($query->current_post == 0) ? 'active' : '';
        $srcVideo = get_post_meta( get_the_ID(), 'video', true );
        $hrefProducto =  get_post_meta( get_the_ID(), 'hrefProducto', true ); 
        $srcImagen = get_the_post_thumbnail_url(get_the_ID(),'full'); 
               
        if($srcVideo != "") {

            $html .= '<div class="carousel-item '.$classActive.'">';

            $html .= '<div class="view">';

             $html .= '<video muted autoplay loop playsinline>
                            <source src="'.$srcVideo.'" type="video/mp4">
                       </video>';

        }else{
            $html .= '<div class="carousel-item '.$classActive.'" style="background-image:url('.$srcImagen.')">';

            $html .= '<div class="view">';
        }
       
        $html .= '<div class="full-bg-img flex-center mask white-text">
                <ul class="animated fadeInUp col-md-12 list-unstyled list-inline">
                <li>
                    <h1 class="font-weight-bold text-uppercase">'. get_the_Title()  . '</h1>
                </li>
                <li>
                    <p class="font-weight-bold text-uppercase py-4">'. get_the_excerpt()  . '</p>
                </li>               
                <li class="list-inline-item">
                    <a target="_blank" href="'. $hrefProducto.'" class="btn btn-cyan btn-lg btn-rounded ml-0"><i class="fas fa-info left"></i>Detalles</a>
                </li>
                </ul>
            </div>
            </div>
        
        </div>';        
    }
    wp_reset_query();
    echo $html;
}

add_action( 'generate_header-categorias-home', 'header_categorias_home', 2 );
function header_categorias_home() {

    $product_categories = getCategorias(5);        

    $html = '';      

        if( !empty($product_categories) ){

            foreach ($product_categories as $key => $category) {
                $classActive = ($key == 0) ? 'active' : '';
                $html .= '<li class="nav-item '.$classActive.'">';
                    $html .= '<a class="nav-link" href="'.get_term_link($category).'">'; 
                        $html .= $category->name;               
                    $html .= '</a>';
                $html .= '</li>';
            }
        }

    echo $html;
}

add_action( 'generate_seccion_category', 'seccion_category', 2 );
function seccion_category() {

    $product_categories = getCategorias(0);        

        if( !empty($product_categories) ){

            foreach ($product_categories as $key => $category) {
                
                $cat_thumb_id = get_term_meta($category->term_id, 'thumbnail_id', true);
                $cat_thumb_url = wp_get_attachment_image_src($cat_thumb_id, 'shop_catalog');
                $cat_thumb_link = $cat_thumb_url[0];
                $imagen = "";
                if($cat_thumb_link !=""){
                    $image = "<img src=".$cat_thumb_link." class='card-img-top' alt=''>";
                }

                $html .= '<div class="card">
                            '.$image.'
                            <div class="card-body">
                                <h5 class="card-title"><a href="'.get_term_link($category).'">'.$category->name.'</a></h5>
                                <p class="card-text">'.$category->description.'</p>
                                <a href="'.get_term_link($category).'" class="btn btn-pink waves-effect waves-light">
                                    <i class="fas fa-clone left"></i> DESCUBRE!!!</a>
                            </div>
                        </div>';  
            }
            $html .= '</div>';
        }
       
    echo $html;
}


add_action( 'generate_seccion_products', 'generate_seccion_products', 2 );
function generate_seccion_products() {
   
    $products = getProducts();
    //echo'<pre>'; print_r($products); die;

    $listLimites = [0,4,8,12,16,20];
    $listLimitesCierre = [3,7,11,15,19,23];
    $html = "";
    foreach ($products as $key => $product) {
        
        $classActive = ($key == 0) ? 'active' : '';

        if(in_array($key, $listLimites)){
            $html .='<div class="carousel-item carousel-item-product '.$classActive.'">';
        }

        $html .= '<div class="col-12 col-sm-6 col-lg-3">
                    <div class="card mb-2">
                    <img class="card-img-top"
                        src="'.$product['Imagen'].'"
                        alt="imagen">
                    <div class="card-body">
                        <h4 class="card-title"><strong>'.$product['Name'].'</strong></h4>
                        <p class="card-text">'.$product['Description'].'</p>                        
                        <div class="woocommerce"><ul class="products"><li class="product"><span class="price">'.$product['price_html'].'</span></li></ul></div>
                        <a href="'.$product['href'].'" class="btn btn-primary"><i class="fas fa-info left"></i>Detalle</a>
                    </div>
                    </div>
                </div>';


        if(in_array($key, $listLimitesCierre)){
            $html .='</div>';
        }       

    }
    echo $html;
}


add_action( 'generate_menu_page_inside', 'generate_menu_page_inside', 2 );

function generate_menu_page_inside() {
    global $woocommerce;

    $inside_logo = esc_url( apply_filters( 'generate_retina_logo', generate_get_option( 'retina_logo' ) ) );
    //$href_logo = esc_url( apply_filters( 'generate_logo_href', home_url( '/' ) ) );
    $title_logo = esc_attr( apply_filters( 'generate_logo_title', get_bloginfo( 'name', 'display' ) ) );
    
    $shop_page_url = get_permalink( woocommerce_get_page_id( 'shop' ) );  
    $cart_url = $woocommerce->cart->get_cart_url();   
    $home_path = $siteurl = set_url_scheme( get_option( 'siteurl' ) ); 

    $product_categories = getCategorias(5);  

    $htmlCategorias = "";
    if( !empty($product_categories) ){

        foreach ($product_categories as $key => $category) {
            $classActive = ($key == 0) ? 'active' : '';
            $htmlCategorias .= '<li class="nav-item '.$classActive.'">';
                $htmlCategorias .= '<a class="nav-link" href="'.get_term_link($category).'">'; 
                    $htmlCategorias .= $category->name;               
                $htmlCategorias .= '</a>';
            $htmlCategorias .= '</li>';
        }
    }

    $html .='<nav class="navbar navbar-inside navbar-expand-lg navbar-light bg-light fixed-top">

            <div class="container justify-content-between">
                          
                <a class="navbar-brand me-2 mb-1 d-flex align-items-center" href="'.$home_path.'" title="'.$title_logo.'" rel="home">
                    <img style="width:190px" src="'.$inside_logo.'" />
                </a>
                
                     
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav" aria-controls="basicExampleNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
                </button>

        <div class="d-block display-contents-pc">
         <div class="collapse navbar-collapse" id="basicExampleNav">
           
                  <ul class="navbar-nav mr-auto ml-2">
                  '.$htmlCategorias.' 
                  </ul>  
           
          </div>
        </div>  
          
        <div class="d-block display-contents-pc">
            <ul class="navbar-nav flex-row items-right-inside"> 
                
                <li class="nav-item me-3 me-lg-1">
                    <a class="nav-link" href="'.$cart_url .'">
                        <i class="fas fa-cart-plus"></i>
                        <span class="badge rounded-pill badge-notification bg-danger total-product-cart">'.WC()->cart->get_cart_contents_count().'</span>
                    </a>   
                </li>                                           
            </ul>          

        </div>

       
        <div class="d-none d-lg-block">
            <a class="btn btn-primary" href="'.$shop_page_url.'" role="button"></i>Comparar</a>
        </div>
         
          
         

    </div>
</nav>';

    echo $html;
}


add_action( 'get_prensa_seccion', 'get_prensa_seccion', 2 );
function get_prensa_seccion() {

        $args = array(  
            'post_type' => 'prensa_item',
            'post_status' => 'publish',
            'posts_per_page' => 8, 
            'orderby' => 'title', 
            'order' => 'ASC', 
        );        
        $query = new WP_Query( $args );

    $html = '';
    $html .= '<div class="row">';
    while ( $query->have_posts() ) {           
        $query->the_post();
       
        
        $title = get_the_Title(); 
        $srcImagen = get_the_post_thumbnail_url(get_the_ID(),'full'); 
        
        $linkPrensa = get_post_meta(get_the_ID(), 'linkPrensa', true);
               
        $html .= '<div class="col-12 col-sm-6 col-md-3 col-lg-2">';
            $html .= '<h6><strong>'.$title.'</strong></h6>';
            $html .= '<a href="'.$linkPrensa.'" target="_blank" rel="noopener noreferrer nofollow"><img src="'.$srcImagen.'"/></a>';        
        $html .= '</div>';    
    }
    $html .= '</div>';
    wp_reset_query();
    echo $html;
}

add_action( 'get_artistas_seccion', 'get_artistas_seccion', 2 );
function get_artistas_seccion() {

        $args = array(  
            'post_type' => 'artista',
            'post_status' => 'publish',
            'posts_per_page' => 8, 
            'orderby' => 'title', 
            'order' => 'ASC', 
        );        
        $query = new WP_Query( $args );

    $html = '';
    $html .= '<div class="row">';
    while ( $query->have_posts() ) {           
        $query->the_post();
                
        $title = get_the_Title(); 
        $srcImagen = get_the_post_thumbnail_url(get_the_ID(),'full');         
        $href = get_post_permalink(get_the_ID());
               
        $html .= '<div class="chip chip-lg">';
            $html .= '<a href="'.$href.'"><img src="'.$srcImagen.'" alt="Contact Person">'.$title.'</a>';
        $html .= '</div>';
           
    }
    $html .= '</div>';
    wp_reset_query();
    echo $html;
}


/**************************Funciones privadas************************************************/
function getCategorias($cant = 0){

    global $wp_query;
    $cat = $wp_query->get_queried_object();
    $orderby = 'id';
    $order = 'desc';
    $hide_empty = true;
    $cat_args = array(
        'orderby'    => $orderby,
        'order'      => $order,
        'hide_empty' => $hide_empty,
        'number' => $cant,
        'parent' => 0
    );
    
    $product_categories = get_terms( 'product_cat', $cat_args );

    return $product_categories;
}

function getProducts(){

    $full_product_list = array();
    $loop = new WP_Query(array('post_type' => array('product'), 'posts_per_page' => -1));
    
    

        while ($loop->have_posts()) : $loop->the_post();
            $theid = get_the_ID();
           
            $product = new WC_Product($theid);
           
                $sku = get_post_meta($theid, '_sku', true);
               
                $selling_price = get_post_meta($theid, '_sale_price', true);
                $regular_price = get_post_meta($theid, '_regular_price', true);
                $content=get_the_content();
                $price_html = $product->get_price_html();
                $description = get_the_excerpt();
                $imagen = get_the_post_thumbnail_url($theid,'full'); 
                $images_ids = $product->get_gallery_image_ids();
                $href = get_post_permalink($theid);

                $images_arr = [];
                for($i = 0, $j = count($images_ids); $i < $j;$i++ ){
                    $image_query = wp_get_attachment_image_src($images_ids[$i]);                  
                    array_push($images_arr, $image_query[0]);
                } 
                $thetitle = get_the_title();
    
            
                $full_product_list[] = array("ID" => (int) $theid,"Description"=> $description,
                   "content" => $content, "Name" => $thetitle, "sku" => $sku,
                    "RegularPrice" => $regular_price, "SellingPrice" => $selling_price,
                    "Imagen" => $imagen, "Imagenes" => $images_arr, "href" => $href, "price_html" => $price_html);
        endwhile;     
        return $full_product_list;
}


/***************************************** Types de post ***********************************************************/
  function info_item() {
    $args = array();
    $labels = array(
        'name'               => _x( 'Info items', 'post type general name' ),
        'singular_name'      => _x( 'Info item', 'post type singular name' ),
        'menu_name'          => 'Info relevante'
        );
        $args = array(
        'labels'        => $labels,
        'description'   => 'Para las informaciones relevantes',
        'public'        => true,
        'menu_position' => 5,
        'supports'      => array( 'title', 'excerpt' ),
        'has_archive'   => true,
        );

    register_post_type( 'info_item', $args );
 }    
  add_action( 'init', 'info_item' );


  function slider_item() {
    $args = array();
    $labels = array(
        'name'               => _x( 'Info Slider', 'post type general name' ),
        'singular_name'      => _x( 'Info Slider', 'post type singular name' ),
        'menu_name'          => 'Slider'
        );
        $args = array(
        'labels'        => $labels,
        'description'   => 'Para las informaciones de los sliders',
        'public'        => true,
        'menu_position' => 5,
        'supports'      => array( 'title', 'editor','thumbnail','custom-fields'),
        'has_archive'   => true,
        );

    register_post_type( 'slider_item', $args );
 }    
  add_action( 'init', 'slider_item' );

  function prensa_item() {
    $args = array();
    $labels = array(
        'name'               => _x( 'Articulo de Prensa', 'post type general name' ),
        'singular_name'      => _x( 'Articulo de Prensa', 'post type singular name' ),
        'menu_name'          => 'Prensa'
        );
        $args = array(
        'labels'        => $labels,
        'description'   => 'Para las informaciones de los articulos de prensa',
        'public'        => true,
        'menu_position' => 5,
        'supports'      => array( 'title','thumbnail','custom-fields','editor'),
        'has_archive'   => true,    
        );

    register_post_type( 'prensa_item', $args );
 }    
  add_action( 'init', 'prensa_item' );



  function artista() {
    $args = array();
    $labels = array(
        'name'               => _x( 'Artista', 'post type general name' ),
        'singular_name'      => _x( 'Artista', 'post type singular name' ),
        'menu_name'          => 'Artista'
        );
        $args = array(
        'labels'        => $labels,
        'description'   => 'Para las informaciones de los artistas',
        'public'        => true,
        'menu_position' => 5,
        'supports'      => array( 'title','thumbnail','custom-fields','editor','excerpt'),
        'has_archive'   => true, 
        );

    register_post_type( 'artista', $args );
    register_taxonomy_for_object_type( 'product_cat', 'artista');
 }    
  add_action( 'init', 'artista' );

add_filter( 'register_taxonomy_args', 'my_taxonomy_args', 10, 2 );
function my_taxonomy_args( $args, $taxonomy_name ) {
 
    if ( 'product_cat' === $taxonomy_name ) {
        $args['show_in_rest'] = true;
    }
 
    return $args;
}
  
/***************************************** Types de post ***********************************************************/



/*
function wpse_allowedtags() {
    // Add custom tags to this string
        return '<script>,<style>,<br>,<em>,<i>,<ul>,<ol>,<li>,<a>,<p>,<img>,<video>,<audio>'; 
    }

if ( ! function_exists( 'wpse_custom_wp_trim_excerpt' ) ) : 

    function wpse_custom_wp_trim_excerpt($wpse_excerpt) {
    $raw_excerpt = $wpse_excerpt;
        if ( '' == $wpse_excerpt ) {

            $wpse_excerpt = get_the_content('');
            $wpse_excerpt = strip_shortcodes( $wpse_excerpt );
            $wpse_excerpt = apply_filters('the_content', $wpse_excerpt);
            $wpse_excerpt = str_replace(']]>', ']]&gt;', $wpse_excerpt);
            $wpse_excerpt = strip_tags($wpse_excerpt, wpse_allowedtags()); /*IF you need to allow just certain tags. Delete if all tags are allowed 

            //Set the excerpt word count and only break after sentence is complete.
                $excerpt_word_count = 75;
                $excerpt_length = apply_filters('excerpt_length', $excerpt_word_count); 
                $tokens = array();
                $excerptOutput = '';
                $count = 0;

                // Divide the string into tokens; HTML tags, or words, followed by any whitespace
                //preg_match_all('/(<[^>]+>|[^<>\s]+)\s*u', $wpse_excerpt, $tokens);

                foreach ($tokens[0] as $token) { 

                    if ($count >= $excerpt_length && preg_match('/[\,\;\?\.\!]\s*$/uS', $token)) { 
                    // Limit reached, continue until , ; ? . or ! occur at the end
                        $excerptOutput .= trim($token);
                        break;
                    }

                    // Add words to complete sentence
                    $count++;

                    // Append what's left of the token
                    $excerptOutput .= $token;
                }

            $wpse_excerpt = trim(force_balance_tags($excerptOutput));

                $excerpt_end = ' <a href="'. esc_url( get_permalink() ) . '">' . '&nbsp;&raquo;&nbsp;' . sprintf(__( 'Read more about: %s &nbsp;&raquo;', 'wpse' ), get_the_title()) . '</a>'; 
                $excerpt_more = apply_filters('excerpt_more', ' ' . $excerpt_end); 

                //$pos = strrpos($wpse_excerpt, '</');
                //if ($pos !== false)
                // Inside last HTML tag
                //$wpse_excerpt = substr_replace($wpse_excerpt, $excerpt_end, $pos, 0); /* Add read more next to last word 
                //else
                // After the content
                $wpse_excerpt .= $excerpt_more; /*Add read more in new paragraph *

            return $wpse_excerpt;   

        }
        return apply_filters('wpse_custom_wp_trim_excerpt', $wpse_excerpt, $raw_excerpt);
    }

endif; 

remove_filter('get_the_excerpt', 'wp_trim_excerpt');
add_filter('get_the_excerpt', 'wpse_custom_wp_trim_excerpt'); */


