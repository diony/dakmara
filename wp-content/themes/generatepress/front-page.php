<?php
 get_header();
?>

<!--Navbar-->
<nav class="navbar navbar-expand-lg navbar-light scrolling-navbar fixed-top">

    <?php 
          global $woocommerce;
          $shop_page_url = get_permalink( woocommerce_get_page_id( 'shop' ) );  
          $cart_url = $woocommerce->cart->get_cart_url();
          $home_path = $siteurl = set_url_scheme( get_option( 'siteurl' ) );
    ?>

    <div class="container">
          <!-- Navbar brand -->
          <a class="navbar-brand" href="<?php print $home_path; ?>" ><img style="width:190px" src="http://tienda.dakmara.com/wp-content/themes/generatepress/assets/img/logo.jpeg" /></a>

          <!-- Collapse button -->

          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav" aria-controls="basicExampleNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="d-block display-contents-pc">
              <div class="collapse navbar-collapse" id="basicExampleNav">

              
                      <ul class="navbar-nav mr-auto ml-2">
                          <?php do_action( 'generate_header-categorias-home' ); ?>
                      </ul>
                          
              </div>
          </div>

          <ul class="navbar-nav flex-row items-right-inside"> 
              <li class="nav-item me-3 me-lg-1">
                  <a class="nav-link" href="<?php print $cart_url ?>">
                      <i class="fas fa-cart-plus"></i>
                      <span class="badge rounded-pill badge-notification bg-danger total-product-cart"><?php print WC()->cart->get_cart_contents_count() ?></span>
                  </a>   
              </li>                                           
          </ul>
          <div class="d-none d-lg-block">
            <a class="btn btn-primary" href="<?php print  $shop_page_url; ?>" role="button"></i>Comparar</a>
        </div>
         
          


    </div>
</nav>
<!--/.Navbar-->


<div id="carousel-example-1" class="carousel slide carousel-fade" data-ride="carousel" data-interval="false">


    <ol class="carousel-indicators">
      <?php do_action( 'get_sliders_seccion_pager' ); ?>
    </ol>

    <div class="carousel-inner" role="listbox">
          <?php 
            do_action( 'get_sliders_seccion' );
          ?>
    </div>

    <a class="carousel-control-prev" href="#carousel-example-1" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carousel-example-1" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
</div>



<section class="cuadros-categorias"> 
  <div class="container">
    <div class="row">
        <div class="col-md-4 col-xs-12">
            <div class="titulo-categorias">
              <h2 class="text-center">Tienda online Dakmara</h2>
              <p class="text-center w-responsive mx-auto ">Descubra el mundo desde sus categorías</p>
            </div>
        </div>
        <div class="col-md-8 col-xs-12">
            <div class="card-columns">
                <?php 
                  do_action( 'generate_seccion_category' );
                ?>
            </div>
        </div>
    </div>
 
</section>

<section class="cuadros-productos"> 
  <div class="container">
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="titulo-seccion-cuadros">
              <h2 class="text-center">Su Galería de Productos en Línea</h2>
              <p class="text-center w-responsive mx-auto ">Nuestra sugerencias para usted</p>
            </div>        
        </div>         
        <div class="col-md-12 col-xs-12">
          
            <!--Carousel Wrapper-->
              <div id="multi-item-example" class="carousel slide carousel-multi-item" data-ride="carousel">

                <!--Controls-->
                <div class="controls-top">
                  <a class="btn-floating" href="#multi-item-example" data-slide="prev"><i class="fas fa-chevron-left"></i></a>
                  <a class="btn-floating" href="#multi-item-example" data-slide="next"><i
                      class="fas fa-chevron-right"></i></a>
                </div>
                <!--/.Controls-->


                <!--Slides-->
                <div class="carousel-inner" role="listbox">

                  <?php 
                    do_action( 'generate_seccion_products' );
                  ?>

                </div>
            

              </div>
                
        </div>
    </div>
  </div>
</section>   

<section class="artistas"> 
  <div class="container">

      <div class="row">
            
           <div class="col-md-4 col-xs-12">
                <div class="titulo-seccion-cuadros">
                  <h2 class="text-center">Nuestros artistas en linea!</h2>                 
              </div>        
            </div>  

            <div class="col-md-8 col-xs-12"> 
              
                <?php 
                  do_action( 'get_artistas_seccion' );
                ?>   
               
           </div>
      </div>
  </div>
</section>   


<section class="contactos">

  <div class="container">

    <h2 class="h1-responsive font-weight-bold text-center my-4">Contactenos</h2>

    <p class="text-center w-responsive mx-auto mb-5">Información a los usuarios de porque es bueno entrar en contacto</p>

    <div class="row">
     
        <div class="col-md-6 mb-md-0 mb-5">
            <form id="contact-form" name="contact-form" action="mail.php" method="POST">
             
                <div class="row">
                   
                    <div class="col-md-6">
                        <div class="md-form mb-0">
                            <input type="text" id="name" name="name" class="form-control">
                            <label for="name" class="">Nombre</label>
                        </div>
                    </div>                  
                    <div class="col-md-6">
                        <div class="md-form mb-0">
                            <input type="text" id="email" name="email" class="form-control">
                            <label for="email" class="">Correo electrónico</label>
                        </div>
                    </div>           
                </div>
              
                <div class="row">
                    <div class="col-md-12">
                        <div class="md-form mb-0">
                            <input type="text" id="subject" name="subject" class="form-control">
                            <label for="subject" class="">Tema</label>
                        </div>
                    </div>
                </div>               
                <div class="row">                   
                    <div class="col-md-12">

                        <div class="md-form">
                            <textarea type="text" id="message" name="message" rows="2" class="form-control md-textarea"></textarea>
                            <label for="message">Mensaje</label>
                        </div>

                    </div>
                </div>
             

            </form>

            <div class="text-center text-md-left">
                <a class="btn btn-primary">Enviar</a>
            </div>
            <div class="status"></div>
        </div>
      
        <div class="col-md-6 text-center">
            <ul class="list-unstyled mb-0">
            
                <li><i class="fas fa-map-marker-alt fa-2x"></i>
                    <p>DAKMARA PRODUCCIONES Y COMERCIO EXTERIOR, S.L.</p>
                </li>

                <li>
                    <p>Calle Aministia, Num. 1
                    <span>Planta 2, Puerta CN</span>
                    <span>28013, Madrid España</span></p>
                </li>

                <li><i class="fas fa-phone mt-4 fa-2x"></i>
                    <p>+34 663478667</p>
                </li>

                <li><i class="fas fa-envelope mt-4 fa-2x"></i>
                    <p>contact@dakmara.com</p>
                </li>
            </ul>
        </div>      
    </div>
  
    </div>

</section>

<section class="prensa"> 
  <div class="container-fluid">

      <div class="row">
            
            <div class="col-md-12 col-xs-12">          
              <h2 class="h1-responsive font-weight-bold text-center my-4">Prensa Digital</h2>
              <p class="text-center w-responsive mx-auto ">Visita lo que dicen de nuestros cuadros internacionalmente </p>
           </div>

           <div class="col-md-12 col-xs-12">
                          <div class="press-list text-center">

                          <?php 
                              do_action( 'get_prensa_seccion' );
                            ?>                           
                        </div>    
            </div>  
      </div>

  </div>
</section>   

<?php 
get_footer();
?>
