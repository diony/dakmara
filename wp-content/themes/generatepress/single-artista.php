<?php
/**
 * The Template for displaying all single posts.
 *
 * @package GeneratePress
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header(); ?>

<div id="primary" <?php generate_do_element_classes( 'content' ); ?>>
		<main id="main" <?php generate_do_element_classes( 'main' ); ?>>

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> <?php generate_do_microdata( 'article' ); ?>>
				<div class="inside-article">

				<div class="row">
					<div class="col-12 col-md-4">
						<?php  $srcImagen = get_the_post_thumbnail_url(get_the_ID(),'full');  ?>
						<img src="<?php print $srcImagen ?>"/>
					</div>
					<div class="col-12 col-md-8">
							<?php					

								if ( generate_show_title() ) {
									$params = generate_get_the_title_parameters();

									the_title( $params['before'], $params['after'] );
								}				
							?>
							<?php the_excerpt(); ?>

							<?php 								
								$link = "#";
								$terms = get_the_terms( get_the_ID(), 'product_cat' );
								if(!empty($terms[0])){
									$link = get_term_link( $terms[0]->term_id, 'product_cat' );
								}								
							?>
							<a class="btn btn-primary" href="<?php echo $link; ?>" role="button">Ver productos</a>
					</div>
				</div>
				<div class="row">
					<div class="col-12 mt-4">

						<div class="entry-content"<?php echo $itemprop; // phpcs:ignore -- No escaping needed. ?>>
							<?php
							the_content();

							wp_link_pages(
								array(
									'before' => '<div class="page-links">' . __( 'Pages:', 'generatepress' ),
									'after'  => '</div>',
								)
							);
							?>
						</div>

					</div>
				</div>


					

					
				</div>
			</article>
		</main>
</div>

	
<?php get_footer();
