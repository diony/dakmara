<?php
/**
 * Footer elements.
 *
 * @package GeneratePress
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( ! function_exists( 'generate_construct_footer' ) ) {
	add_action( 'generate_footer', 'generate_construct_footer' );
	/**
	 * Build our footer.
	 *
	 * @since 1.3.42
	 */
	function generate_construct_footer() {
		$inside_site_info_class = '';

		if ( 'full-width' !== generate_get_option( 'footer_inner_width' ) ) {
			$inside_site_info_class = ' grid-container grid-parent';

			if ( generate_is_using_flexbox() ) {
				$inside_site_info_class = ' grid-container';
			}
		}
		?>
		
		
		<!--Footer-->
		<footer class="page-footer text-center text-md-left pt-4">
		<!--Footer Links-->
		<div class="container-fluid">
		<div class="row">
			<!--First column-->
			<div class="col-md-3 offset-md-1">
				<h5 class="font-weight-bold text-uppercase mb-4">Contactenos</h5>
				<p>DAKMARA PRODUCCIONES Y COMERCIO EXTERIOR, S.L.</p>

				<p>Calle Aministia, Num. 1 Planta 2, Puerta CN 28013, Madrid España</p>

				<p>+34 663478667</p>

				<p>contact@dakmara.com</p>	
			</div>
			<!--/.First column-->
			<hr class="clearfix w-100 d-md-none">
			<!--Second column-->
			<div class="col-md-2 offset-md-1">
			<h5 class="font-weight-bold text-uppercase mb-4">Artistas</h5>
			<ul class="list-unstyled">
				<li><a href="#!">Link 1</a></li>
				<li><a href="#!">Link 2</a></li>
				<li><a href="#!">Link 3</a></li>
				<li><a href="#!">Link 4</a></li>
			</ul>
			</div>
			<!--/.Second column-->
			<hr class="clearfix w-100 d-md-none">
			<!--Third column-->
			<div class="col-md-2">
			<h5 class="font-weight-bold text-uppercase mb-4">Categorías</h5>
			<ul class="list-unstyled">
				<li><a href="#!">Link 1</a></li>
				<li><a href="#!">Link 2</a></li>
				<li><a href="#!">Link 3</a></li>
				<li><a href="#!">Link 4</a></li>
			</ul>
			</div>
			<!--/.Third column-->
			<hr class="clearfix w-100 d-md-none">
			<!--Fourth column-->
			<div class="col-md-2">
			<h5 class="font-weight-bold text-uppercase mb-4">Colecciones</h5>
			<ul class="list-unstyled">
				<li><a href="#!">Link 1</a></li>
				<li><a href="#!">Link 2</a></li>
				<li><a href="#!">Link 3</a></li>
				<li><a href="#!">Link 4</a></li>
			</ul>
			</div>
			<!--/.Fourth column-->
		</div>
		</div>
		<!--/.Footer Links-->
		<hr>
		<!--Call to action-->
		<div class="call-to-action text-center">
		<h4 class="my-4">Siguenos en las redes sociales</h4>
		<ul class="list-unstyled list-inline mb-4">
			<li>
			<h5 class="my-4"></h5></li>
			<li class="list-inline-item"><a target="_blank" href="https://mdbootstrap.com/getting-started/" class="btn btn-info">Facebook</a></li>
			<li class="list-inline-item"><a target="_blank" href="https://mdbootstrap.com/material-design-for-bootstrap/" class="btn btn-default">Instagram</a></li>
		</ul>
		</div>
		<!--/.Call to action-->
		<!--Copyright-->
		<div class="footer-copyright text-center">
		<div class="container-fluid py-3">
			© 2021 Copyright: <a href="https://DAKMARA.com"> DAKMARA PRODUCCIONES Y COMERCIO EXTERIOR, S.L.</a>
		</div>
		</div>
		<!--/.Copyright-->
		</footer>
		<!--/.Footer-->
		<?php
	}
}
