<?php
/**
 * Plugin Name: Utiles
 * Plugin URI: https://www.conocerde.com
 * Description: descripcion.
 * Author: diony
 * Text Domain: utiles
 * Domain Path: /languages/
 * Version: 1.0.0
 * WC tested up to: 4.3.2
 * Author URI: http://www.conocerde.com
 *
 * @package WordPress
 * @author Diony
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if( !defined( 'UTILES_VERSION' ) ) {
    define( 'UTILES_VERSION', '1.0.0' ); // Version of plugin
}
if( !defined( 'UTILES_DIR' ) ) {
    define( 'UTILES_DIR', dirname( __FILE__ ) ); // Plugin dir
}
if( !defined( 'UTILES_URL' ) ) {
    define( 'UTILES_URL', plugin_dir_url( __FILE__ ) ); // Plugin url
}


/**
 * Load the plugin after the main plugin is loaded.
 *
 * @package Utiles
 * @since 1.0.0
 */
function utiles_load_plugin() {


    function utiles_style() {

        //CSS
        wp_enqueue_style( 'utiles_style_css',  plugin_dir_url( __FILE__ ) . 'assets/css/utiles.css', array(), UTILES_VERSION );

        //script
        wp_register_script( 'utiles_js', UTILES_URL.'assets/js/utiles.js', array('jquery'), UTILES_VERSION, true );
        wp_enqueue_script( 'utiles_js' );
    }

    add_action( 'wp_enqueue_scripts', 'utiles_style' );

    // Including some files
    //require_once( 'class-categorias-shortcode.php' );

}


/*********************************** Formulario personalizado en el detalle de un producto ****************************/

/*add_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart_personalizada', 30);

function woocommerce_template_single_add_to_cart_personalizada()
{

    global $product;
    $html = "";
    if ($product && $product->is_type('simple') && $product->is_purchasable() && $product->is_in_stock()) {

        $html .= '<form action="' . esc_url($product->add_to_cart_url()) . '" class="cart">';
        $html .= woocommerce_quantity_input(array(), $product, false);
        $html .= '<input type="hidden" name="product_id" value="' . $product->id . '" id="product_id"> ';
        $html .= '<button type="button" class="button alt add-cart-page-simple">' . esc_html($product->add_to_cart_text()) . '</button>';
        $html .= '</form>';

    }
    //echo'<pre>'; print_r($html); die;
    echo $html;
}*/

/*********************************** Action que devuelve las fregmentos de woocomerce *************************/
function get_fragments_woocomerce_from_cart(){

    ob_start();
    WC_AJAX:: get_refreshed_fragments();
    wp_die();
}

add_action('wp_ajax_get_fragments_woocomerce_from_cart', 'get_fragments_woocomerce_from_cart');
add_action('wp_ajax_nopriv_get_fragments_woocomerce_from_cart', 'get_fragments_woocomerce_from_cart');



/*********************************** Action para llamar cuando se aumenta o disminuye la cantidad en el poput *******/
/*function update_item_from_cart()
{

    $quantity = $_POST['qty'];

    // Get mini cart
    ob_start();

    foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {
        if ($cart_item_key == $_POST['cart_item_key']) {
            WC()->cart->set_quantity($cart_item_key, $quantity, $refresh_totals = true);
        }
    }
    WC()->cart->calculate_totals();
    WC()->cart->maybe_set_cart_cookies();

    $cantidad = WC()->cart->get_cart_contents_count();
    woocommerce_detalle_cart();
    $mini_cart = ob_get_clean();
    $detalle = $mini_cart;

    $array = array($cantidad, $detalle);
    echo json_encode($array);

    wp_die();

}

add_action('wp_ajax_update_item_from_cart', 'update_item_from_cart');
add_action('wp_ajax_nopriv_update_item_from_cart', 'update_item_from_cart');


/*********************************** Añade fragmento de información en el woocomerce *******

add_filter('woocommerce_add_to_cart_fragments', function ($fragments) {

    ob_start();

    woocommerce_detalle_cart();

    $mini_cart = ob_get_clean();

    $fragments['cart-detalle-contents'] = $mini_cart;

    return $fragments;

});


if (!function_exists('woocommerce_detalle_cart')) {

    function woocommerce_detalle_cart($args = array())
    {

        $defaults = array(
            'list_class' => '',
        );

        $args = wp_parse_args($args, $defaults);

        wc_get_template('cart/detalle-cart.php', $args);
    }
}


/************************* Action para añadir un producto a woocomerce desde el formulario del detalle del producto***

add_action('wp_ajax_woocommerce_ajax_add_to_cart', 'woocommerce_ajax_add_to_cart');
add_action('wp_ajax_nopriv_woocommerce_ajax_add_to_cart', 'woocommerce_ajax_add_to_cart');

function woocommerce_ajax_add_to_cart()
{

    $product_id = apply_filters('woocommerce_add_to_cart_product_id', absint($_POST['product_id']));
    $quantity = empty($_POST['product_qty']) ? 1 : wc_stock_amount($_POST['product_qty']);
    $variation_id = absint($_POST['variation_id']);
    $passed_validation = apply_filters('woocommerce_add_to_cart_validation', true, $product_id, $quantity);

    $product_status = get_post_status($product_id);


    if ($passed_validation && WC()->cart->add_to_cart($product_id, $quantity, $variation_id) && 'publish' === $product_status) {

        do_action('woocommerce_ajax_added_to_cart', $product_id);

        WC_AJAX:: get_refreshed_fragments();

    } else {

        $data = array(
            'error' => true
        );
    }

    echo json_encode($data);
    wp_die();
}*/

// Action to load plugin after the main plugin is loaded
add_action('plugins_loaded', 'utiles_load_plugin', 15);
