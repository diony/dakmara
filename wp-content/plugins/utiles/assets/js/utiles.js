jQuery(document).ready(function($) {


	console.log("dddd");

	if($(".category-header img").length){
		$("body.tax-product_cat .site-main").css('margin',0);
	}

	$(window).scroll(function (event) {
		var scroll = $(window).scrollTop();
		if(scroll > 0){
			$("header").animate(getclass(),50);
		}else{
			$("header").animate(getclass1(),50);
		}
	});

	function getclass(){
		$("header").addClass("menuScrollopen");
		$("header").removeClass("menuScrollclose");
	}

	function getclass1(){
		$("header").addClass("menuScrollclose");
		$("header").removeClass("menuScrollopen");
	}

	console.log(URL_SITE);
	var ajaxscript = { ajax_url : URL_SITE + '/wp-admin/admin-ajax.php' }

	$('body').on( 'removed_from_cart', function(data,data1,data3){

		$('.total-product-cart').html(data1['cart-total-contents']);
		$('.total_cart_header').html(data1['cart-total-contents']);
		$('.contenido-cart').html(data1['cart-detalle-contents']);

		jQuery('body').trigger( 'wc_fragments_refreshed' );


	});

	$('body').on( 'added_to_cart', function(data,data1,data3){

	console.log(data1);

		$('a.added_to_cart').hide();

		$('.total-product-cart').html(data1['cart-total-contents']);
		$('.total_cart_header').html(data1['cart-total-contents']);
		$('.contenido-cart').html(data1['cart-detalle-contents']);

		$('div.detalleCart').show();

	});

	$(document).on('click', '.contenido-cart .qty', function (e){
		e.preventDefault();
		var qty = $(this).val();
		var cart_item_key = $(this).attr("id");

		$.ajax({
			type: 'POST',
			dataType: 'json',
			url: ajaxscript.ajax_url,
			data: {action : 'update_item_from_cart', 'cart_item_key' : cart_item_key, 'qty' : qty },
			success: function (data) {
				console.log(data);
				$('.total-product-cart').html(data[0]);
				$('.total_cart_header').html(data[0]);
				$('.contenido-cart').html(data[1]);
				jQuery('body').trigger( 'wc_fragments_refreshed' );
			},
			error:function(data){
				console.log(data);
			}
		});

	});

	$(document).on('click', '.add-cart-page-simple', function (e){
		e.preventDefault();

		$thisbutton = $(this);
		$form = $thisbutton.closest('form.cart');
		$product_qty = $form.find('input[name=quantity]').val() || 1;
		$product_id = $form.find('input[name=product_id]').val() || id;
		$variation_id = $form.find('input[name=variation_id]').val() || 0;

		$.ajax({
			type: 'POST',
			dataType: 'json',
			url: ajaxscript.ajax_url,
			data: {action : 'woocommerce_ajax_add_to_cart', 'product_id' : $product_id, 'product_qty' : $product_qty },
			success: function (data) {
				$('.total_cart_header').html(data.fragments['cart-total-contents']);
			},
			error:function(data){
				console.log(data);
			}

		});

	});

});

function show_poput(){

	var ajaxscript = { ajax_url : URL_SITE + '/wp-admin/admin-ajax.php' }

	jQuery.ajax({
		type: 'POST',
		dataType: 'json',
		url: ajaxscript.ajax_url,
		data: {action : 'get_fragments_woocomerce_from_cart'},
		success: function (data) {
			console.log(data);
			jQuery('.total-product-cart').html(data.fragments['cart-total-contents']);
			jQuery('.contenido-cart').html(data.fragments['cart-detalle-contents']);
			jQuery('body').trigger( 'wc_fragments_refreshed' );
			jQuery('div.detalleCart').show();
		},
		error:function(data){
			console.log(data);
		}
	});


}